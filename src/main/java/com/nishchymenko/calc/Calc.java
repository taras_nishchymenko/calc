package com.nishchymenko.calc;

import com.nishchymenko.calc.expression.Expression;
import com.nishchymenko.calc.expression.ExpressionConverter;
import com.nishchymenko.calc.util.StringUtil;

import java.util.Scanner;

public class Calc {

    public void launch() {
        showInfo();

        while (true) {
            String input = getString();
            try {
                applyExpression(input);
            } catch (RuntimeException e) {
                System.out.println("Incorrect expression, try again!");
                e.printStackTrace();
            }
        }
    }

    private String getString() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input expression: ");
        String input = scanner.nextLine();

        if (input.equals("q")) {
            System.exit(0);
        }
        return input;
    }

    private void showInfo() {
        System.out.println("Input 'q' to quit");
        System.out.println("You can use:\n" +
                "cos(), sin(), tg(), ctg(), abs(), sqrt(), sqr()\n" +
                "And common operators +, -, *, /, ^");
    }

    private void applyExpression(String input) {
        ExpressionConverter converter = new ExpressionConverter();
        String infixExpression = StringUtil.replaceFunctionsAndValidate(input);
        Expression expression = converter.convertFromInfixToPostfix(infixExpression);
        double result = expression.proceed();
        System.out.println("Result: " + result);
    }
}
