package com.nishchymenko.calc.util;

public class StringUtil {
    public static String replaceFunctionsAndValidate(String expression) {
        expression = expression
                .replaceAll(" ", "")
                .replaceAll("(.*)=", "");

        if (expression.startsWith("-")) {
            expression = expression.replaceFirst("-", "0-");
        }
        return expression
                .replaceAll("sin", "s")
                .replaceAll("cos", "c")
                .replaceAll("tg", "t")
                .replaceAll("ctg", "k")
                .replaceAll("abs", "a")
                .replaceAll("sqrt", "q")
                .replaceAll("sqr", "w")
                .replaceAll("\\(-", "(0-");
    }
}
