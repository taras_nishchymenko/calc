package com.nishchymenko.calc.expression.function;

@FunctionalInterface
public interface OperatorFunctionInterface {
    double apply(double arg1, double arg2);
}
