package com.nishchymenko.calc.expression.function;

import java.util.HashMap;
import java.util.Map;

public class PrefixFunctions {

    private static Map<Character, PrefixFunctionInterface> functions = new HashMap<>();
    static {
        functions.put('s', Math::sin);
        functions.put('c', Math::cos);
        functions.put('t', Math::tan);
        functions.put('k', arg -> 1.0 / Math.tan(arg));
        functions.put('a', Math::abs);
        functions.put('q', Math::sqrt);
        functions.put('w', arg -> Math.pow(arg, 2));
    }

    public static PrefixFunctionInterface get(Character func) {
        return functions.get(func);
    }
}
