package com.nishchymenko.calc.expression.function;

@FunctionalInterface
public interface PrefixFunctionInterface {
    double apply(double arg);
}
