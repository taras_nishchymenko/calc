package com.nishchymenko.calc.expression.function;

import java.util.HashMap;
import java.util.Map;

public class OperatorFunctions {
    private static Map<Character, OperatorFunctionInterface> functions = new HashMap<>();
    static {
        functions.put('+', (arg1, arg2) -> arg1 + arg2);
        functions.put('-', (arg1, arg2) -> arg1 - arg2);
        functions.put('*', (arg1, arg2) -> arg1 * arg2);
        functions.put('/', (arg1, arg2) -> arg1 / arg2);
        functions.put('^', (Math::pow));
    }

    public static OperatorFunctionInterface get(Character func) {
        return functions.get(func);
    }
}
