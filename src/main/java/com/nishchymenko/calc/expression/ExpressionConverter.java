package com.nishchymenko.calc.expression;

import com.nishchymenko.calc.expression.exception.InvalidExpressionException;
import com.nishchymenko.calc.expression.part.*;
import com.nishchymenko.calc.expression.part.Number;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class ExpressionConverter {

    private Queue<Character> inputExpression = new LinkedList<>();
    private Stack<Character> stack = new Stack<>();
    private Expression resultExpression = new Expression();

    public Expression convertFromInfixToPostfix(String infixExpression) throws RuntimeException {
        for (Character character : infixExpression.toCharArray()) {
            inputExpression.add(character);
        }
        buildPostfixExpression();
        return resultExpression;
    }

    private void buildPostfixExpression() {
        while (!inputExpression.isEmpty()) {
            Character character = inputExpression.poll();
            proceedCurrentCharacter(character);
        }
        addOperators();
    }

    private void addOperators() {
        while (!stack.isEmpty()) {
            resultExpression.push(ExpressionPart.fromCharacter(stack.pop()));
        }
    }

    private void proceedCurrentCharacter(Character character) {
        if (!ExpressionPartUtil.isExpressionPart(character)) {
            throw new InvalidExpressionException();
        } else if (ExpressionPartUtil.isNumberPart(character)) {
            getNumberFromQueue(character);
        } else if (ExpressionPartUtil.isPostfixFunction(character)) {
            resultExpression.push(new PostfixFunction(character));
        } else if (ExpressionPartUtil.isPrefixFunction(character)) {
            stack.push(character);
        } else if (ExpressionPartUtil.isOpenBracket(character)) {
            stack.push(character);
        } else if (ExpressionPartUtil.isCloseBracket(character)) {
            placeOperatorsInResult();
        } else if (ExpressionPartUtil.isOperator(character)) {
            placeBinaryOperators(character);
        }
    }

    private void getNumberFromQueue(Character numberPart) {
        StringBuilder builder = new StringBuilder();
        builder.append(numberPart);

        while (!inputExpression.isEmpty() && ExpressionPartUtil.isNumberPart(inputExpression.peek())) {
            builder.append(inputExpression.poll());
        }

        double number = Double.parseDouble(builder.toString());
        resultExpression.push(new Number(number));
    }

    private void placeBinaryOperators(Character operator) {
        if (!stack.isEmpty()) {
            boolean graterPriority = stackOperationPriorityGraterThenOperator(operator);
            boolean isPrefixFunction = ExpressionPartUtil.isPrefixFunction(stack.peek());

            while ((!stack.isEmpty() && isPrefixFunction) || (!stack.isEmpty() && graterPriority)) {
                isPrefixFunction = ExpressionPartUtil.isPrefixFunction(stack.peek());
                resultExpression.push(ExpressionPart.fromCharacter(stack.pop()));
                graterPriority = stackOperationPriorityGraterThenOperator(operator);
            }
        }
        stack.push(operator);
    }

    private boolean stackOperationPriorityGraterThenOperator(Character operator) {
        if (!stack.isEmpty()) {
            Character characterFromStack = stack.peek();
            int characterPriority = ExpressionPartUtil.getPriority(operator);
            int characterFromStackPriority = ExpressionPartUtil.getPriority(characterFromStack);
            return characterFromStackPriority >= characterPriority;
        } else {
            return false;
        }
    }

    private void placeOperatorsInResult() {
        while (!stack.isEmpty() && !ExpressionPartUtil.isOpenBracket(stack.peek())) {
            Character character = stack.pop();
            if (ExpressionPartUtil.isOpenBracket(character)) {
                throw new InvalidExpressionException();
            }
            resultExpression.push(ExpressionPart.fromCharacter(character));
        }
        if (!stack.isEmpty() && ExpressionPartUtil.isOpenBracket(stack.peek())) {
            stack.pop();
        }
    }

}
