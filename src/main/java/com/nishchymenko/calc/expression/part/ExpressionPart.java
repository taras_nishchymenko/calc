package com.nishchymenko.calc.expression.part;

public interface ExpressionPart {

    static ExpressionPart fromCharacter(Character character) {
        return ExpressionPartUtil.isPrefixFunction(character) ? new PrefixFunction(character) :
                ExpressionPartUtil.isPostfixFunction(character) ? new PostfixFunction(character) :
                new Operator(character);
    }
}
