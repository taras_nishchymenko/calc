package com.nishchymenko.calc.expression.part;

public class Number implements ExpressionPart {

    private Double number;

    public Number(Double number) {
        this.number = number;
    }

    public Double getNumber() {
        return number;
    }
}
