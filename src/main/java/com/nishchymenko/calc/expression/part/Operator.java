package com.nishchymenko.calc.expression.part;

import com.nishchymenko.calc.expression.function.OperatorFunctions;

public class Operator implements ExpressionPart {

    private Character operator;

    public Operator(Character operator) {
        this.operator = operator;
    }

    public double execute(double firstOperand, double secondOperand) {
        return OperatorFunctions.get(operator).apply(firstOperand, secondOperand);
    }
}
