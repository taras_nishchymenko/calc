package com.nishchymenko.calc.expression.part;

public class ExpressionPartUtil {
    public static boolean isOperator(Character character) {
        return character.equals('+')
                || character.equals('-')
                || character.equals('*')
                || character.equals('/')
                || character.equals('^');
    }

    public static boolean isPrefixFunction(Character character) {
        return character.equals('s')
                || character.equals('c')
                || character.equals('t')
                || character.equals('k')
                || character.equals('a')
                || character.equals('q')
                || character.equals('w');
    }

    public static boolean isOpenBracket(Character character) {
        return character.equals('(');
    }

    public static boolean isExpressionPart(Character character) {
        return isPrefixFunction(character)
                || isPostfixFunction(character)
                || isOperator(character)
                || isBracket(character)
                || isNumberPart(character);
    }

    private static boolean isBracket(Character character) {
        return character.equals('(') || character.equals(')');
    }

    public static boolean isPostfixFunction(Character character) {
        return character.equals('!');
    }

    public static boolean isCloseBracket(Character character) {
        return character.equals(')');
    }

    public static int getPriority(Character character) {
        return isPrefixFunction(character) ? 1 :
                isLowPriorityOperator(character) ? 2 :
                isMiddlePriorityOperator(character) ? 3 :
                isHighPriorityOperator(character) ? 4 : 0;
    }

    private static boolean isHighPriorityOperator(Character character) {
        return character.equals('^');
    }

    private static boolean isMiddlePriorityOperator(Character character) {
        return character.equals('*') || character.equals('/');
    }

    private static boolean isLowPriorityOperator(Character character) {
        return character.equals('-') || character.equals('+');
    }

    public static boolean isNumberPart(Character character) {
        return Character.isDigit(character) || character.equals('.');
    }
}
