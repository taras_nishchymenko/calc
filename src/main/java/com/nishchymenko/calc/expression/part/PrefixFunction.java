package com.nishchymenko.calc.expression.part;

import com.nishchymenko.calc.expression.function.PrefixFunctions;

public class PrefixFunction implements ExpressionPart {

    private Character func;

    public PrefixFunction(char c) {
        func = c;
    }

    public double execute(Double number) {
        return PrefixFunctions.get(func).apply(number);
    }
}
