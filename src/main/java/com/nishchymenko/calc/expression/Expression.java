package com.nishchymenko.calc.expression;

import com.nishchymenko.calc.expression.part.ExpressionPart;
import com.nishchymenko.calc.expression.part.PrefixFunction;
import com.nishchymenko.calc.expression.part.Number;
import com.nishchymenko.calc.expression.part.Operator;

import java.util.LinkedList;

public class Expression {

    LinkedList<ExpressionPart> expression;

    public Expression() {
        expression = new LinkedList<>();
    }

    public void push(ExpressionPart part) {
        expression.add(part);
    }

    public double proceed() {
        int index = 0;

        while (expression.size() != 1) {
            ExpressionPart current = expression.get(index);
            if (current instanceof Operator) {
                index = proceedOperator(current);
            } else if (current instanceof PrefixFunction) {
                index = proceedPrefixFunction(current);
            } else {
                index++;
            }
        }

        Number result = (Number) expression.getFirst();

        return result.getNumber();
    }

    private int proceedPrefixFunction(ExpressionPart current) {
        PrefixFunction prefixFunction = (PrefixFunction) current;

        int index = expression.indexOf(current) - 1;
        Number operand = (Number) expression.get(index);
        double result = prefixFunction.execute(operand.getNumber());
        expression.set(index, new Number(result));
        expression.remove(current);

        return index;
    }

    private int proceedOperator(ExpressionPart current) {
        Operator operator = (Operator) current;

        int firstIndex = expression.indexOf(current) - 2;
        int secondIndex = expression.indexOf(current) - 1;
        Number firstOperand = (Number) expression.get(firstIndex);
        Number secondOperand = (Number) expression.get(secondIndex);

        double resultOfOperation = operator.execute(firstOperand.getNumber(), secondOperand.getNumber());
        expression.set(firstIndex, new Number(resultOfOperation));
        expression.remove(secondIndex);
        expression.remove(current);

        return firstIndex;
    }
}
